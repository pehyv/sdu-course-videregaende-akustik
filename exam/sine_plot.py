# -*- coding: utf-8 -*-
"""
Created on Sat May 25 13:14:42 2019

@author: pette
"""

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import locale
locale.setlocale(locale.LC_NUMERIC, 'da-DK')

sns.set_context('notebook')
sns.set_style('whitegrid')

t = np.linspace(0, 0.005, 200)
freq = 500
s = 0.2*np.sin(2*np.pi*freq*t)

f = plt.figure(figsize=(9/2.54, 6/2.54))
plt.plot(t, s, 'k')
plt.xlim((-0.00001, 0.00502))
plt.ylim((-0.25, 0.25))
plt.title('p(t)')

sns.despine(left=True, right=True, top=False, bottom=False)

ax = plt.gca()
ax.set_xticks([0, 0.001, 0.002, 0.003, 0.004, 0.005])
ax.ticklabel_format(axis='both', useLocale=True)
ax.tick_params(axis='x', labelrotation=-25.)

plt.xlabel('Tid [s]')
plt.ylabel('Lydtryk [Pa]')

plt.savefig('sine_wave.png', bbox_inches='tight', dpi=300)