\documentclass[aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{icomma}
\usepackage{verbatim}
\usetheme{AnnArbor}
\usecolortheme{seagull}

\graphicspath{ {lecture_4/} }

\title{Lecture 4 --- Complex numbers}
\subtitle{Advanced Acoustics / Vidergående Akustik / F19}
\author{Petteri Hyvärinen}
\date{March 4th 2019}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Complex numbers}
\subsection{Background}

\begin{frame}{Complex numbers}
    On this course we will use complex numbers. You will be fine.
\end{frame}

\begin{frame}{Why so complex?}
    \begin{itemize}
        \item<1-> Why complicate things? Can't we just use \textit{uncomplex} numbers and keep things simple?
        \item<2-> We could indeed use only the real numbers ($\mathbb{R}$, which include e.g. $0, 1, -4, 1{,}5538, \frac{1}{3}, \sqrt{2}, \pi, $ \textit{etc.} \ldots) and their functions
        \item<3-> But that would actually lead to very tedious math. Just as a simple example (something we might use when figuring out how two simultaneous tones interact with each other):
    \end{itemize}
    \begin{block}{Sum of two sines}<4->
        $$ \sin \alpha + \sin \beta = 2 \sin\frac{\alpha + \beta}{2} \cos\frac{\alpha - \beta}{2} $$
    \end{block}
    \begin{itemize}
        \item<5-> \ldots and here we don't even consider different amplitudes for the waves
    \end{itemize}
\end{frame}

\begin{frame}{Other ``complex'' mathematical objects}
    \begin{itemize}
        \item<1-> In fact, a large part of what is often considered ``easy'' math, is just as abstract as complex numbers
        \item<2-> For example, where in the real world do you \textbf{see} \textit{zero} of something? Or \textit{negative} things, something that is less than nothing?
        \item<3-> So, math provides us with tools and useful concepts that we can apply to everyday problems, and complex numbers are just one more useful set of tools
    \end{itemize}
\end{frame}

\subsection{Introduction to complex numbers}
\begin{frame}{Imaginary number}
    A complex number (set symbol $\mathbb{C}$) can be written as:
    \begin{block}{Complex number}
    $$z = a + bi,$$
    \end{block}
    where both $a$ and $b$ are real numbers ($a,b \in \mathbb{R}$). \pause
    
    What makes $z$ a complex number is the $i$ that is multiplied with $b$. It is called the \textit{imaginary} number.
    
    \begin{block}{Imaginary number $i$}
    $$i^2 = -1$$
    $$i = +\sqrt{-1}$$
    \end{block}
\end{frame}

\begin{frame}{Imaginary number}
\begin{itemize}
    \item What is special about the imaginary number is that for all real numbers ($x \in \mathbb{R}$), $x^2 \geq 0$, but now we see that $i^2 = -1 < 0$. \pause
    \item This property of the imaginary number is very handy, for example when looking for roots of a polynomial equation
    $$ x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}$$
\end{itemize}

    

\end{frame}

\begin{frame}{Some conventions for writing complex numbers}
    \begin{alertblock}{$i = j$}
    Sometimes, the imaginary number is written as $j$ instead of $i$.
    \end{alertblock}
    \begin{itemize}
        \item Usually it is clear from the context, but sometimes can be confusing
        \item $j$ is used especially in electrical engineering, where $i$ usually stands for electric current
    \end{itemize}
    \pause
    
    \begin{block}{Variable names}
    Often complex-valued variables are given names $z$ and $w$, whereas real-valued variables are named $x$ and $y$
    \end{block}
    \begin{itemize}
        \item Again, this is not always the case, so always check!
        \item $x \in \mathbb{R}$ means that $x$ is real, $x \in \mathbb{C}$ means that $x$ is complex
    \end{itemize}

\end{frame}

\begin{frame}{Real and imaginary parts}
When we want just the real part of a complex number, we use the notation $\operatorname{Re}(\cdot)$, and for the imaginary part $\operatorname{Im}(\cdot)$. So if $z = a + bi$, then
\begin{align*}
    \operatorname{Re}(z) &= a, \text{ and}\\
    \operatorname{Im}(z) &= b
\end{align*}
\pause
If the imaginary part is zero ($b = 0$), then $z$ is a regular, real number. This means that complex numbers include all the real numbers ($\mathbb{R} \subset \mathbb{C}$)

\end{frame}

\begin{frame}{Rectangular form}
\begin{itemize}
    \item<1-> The format $z = a + bi$ is called the \textit{rectangular form} of a complex number
    
    \item<2-> Complex numbers are often visualized as coordinates or vectors in the \textit{complex plane}. This visualization is called an \textit{Argand diagram}
    
    \item<3-> The $a$ and $b$ in the rectangular form correspond to the $x$ and $y$ coordinates in the complex plane, respectively.
    
    \item<4-> We only write the non-zero parts of a complex number,\\ e.g. $5i$ but \textbf{not} $0 + 5i$, and $3$ but \textbf{not} $3 + 0i$.
    
    \item<5-> Exception: $0$ means that both $a$ \textbf{and} $b$ are zero ($0 + 0i$)
    
\end{itemize}
\end{frame}

\begin{frame}{Argand diagram}
    \begin{figure}
        \centering
        \includegraphics<1|handout:0>[width=0.8\textwidth]{complex_rect-1.png}
        \includegraphics<2|handout:0>[width=0.8\textwidth]{complex_rect-2.png}
        \includegraphics<3|handout:0>[width=0.8\textwidth]{complex_rect-3.png}
        \includegraphics<4>[width=0.8\textwidth]{complex_rect-4.png}
    \end{figure}
\end{frame}

\subsection{Complex arithmetic}
\begin{frame}{Complex arithmetic}
    Complex numbers can be added together
    $$ z + w = (a + bi) + (c + di) = (a + c) + (b + d)i, $$
    \pause
    subtracted from each other 
    $$ z - w  = (a + bi) - (c - di) = (a - c) + (b - d)i, $$
    \pause
    (also note that $z - w = z + (-w)$),\pause \\
    and multiplied
    \begin{align*}
        zw &= (a + bi)(c + di) = ac + adi + bic + bidi \\
        &= ac + adi + bci + bdi^2 = ac + adi + bci + bd(-1)\\
        &= (ac - bd) + (ad + bc)i
    \end{align*}

\end{frame}

\begin{frame}{Sum of complex numbers}
\begin{figure}
    \centering
    \includegraphics<1|handout:0>[width=0.8\textwidth]{complex-sum-1.png}
    \includegraphics<2|handout:0>[width=0.8\textwidth]{complex-sum-2.png}
    \includegraphics<3>[width=0.8\textwidth]{complex-sum-3.png}
\end{figure}
\end{frame}

\begin{frame}{Subtracting complex numbers}
\begin{figure}
    \centering
    \includegraphics<1|handout:0>[width=0.8\textwidth]{complex_subtract-1.png}
    \includegraphics<2>[width=0.8\textwidth]{complex_subtract-2.png}
\end{figure}
\end{frame}

\begin{frame}{Complex multiplication}
\begin{figure}
    \centering
    \includegraphics<1|handout:0>[width=0.8\textwidth]{complex_mult-1.png}
    \includegraphics<2>[width=0.8\textwidth]{complex_mult-2.png}
\end{figure}
\end{frame}

\subsection{Polar form}
\begin{frame}{Polar form}
    \begin{itemize}
        \item To have an intuitive interpretation of complex multiplication, it helps to look at another way of writing a complex number
    \end{itemize}
\end{frame}

\begin{frame}{Polar form}
\begin{columns}[t]
\begin{column}{0.5\textwidth}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{complex_rect-polar.png}
\end{figure}
\end{column}
\begin{column}{0.5\textwidth}
\begin{align}
    z &= x + yi \\
    r &= \sqrt{x^2 + y^2} = |z| \\
    \tan \theta &= \frac{y}{x} \nonumber\\
    \Leftrightarrow \theta &= \tan^{-1} \frac{y}{x} \nonumber\\
    &= \arctan \frac{y}{x}
\end{align}
Using $r$ and $\theta$, we can now write $z$ like this:
\begin{block}{Polar form}
$$z = r\cdot (\cos\theta + i\sin\theta)$$
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Magnitude and argument}
\begin{block}{Magnitude of a complex number}
The $r$ in the polar form is called the \textit{magnitude} or \textit{modulus} of a complex number
\end{block}
It corresponds to the length of the vector on the complex plane
\pause
\begin{block}{Argument of a complex number}
The $\theta$ in the polar form is called the \textit{argument} of a complex number
\end{block}
\begin{itemize}
    \item It corresponds to the angle between the positive real axis and the vector on the complex plane
    \item The argument increases counter-clockwise
    \item Negative values of the argument mean that the angle is measured clockwise
\end{itemize}
\end{frame}

\begin{frame}{Magnitude and argument, cont'd}
    The notation $|z|$ is also used for the magnitude. For real numbers, it is just the absolute value of the number.\pause\\
    One property of the $|\cdot|$ operation is $|zw| = |z|\cdot|w|$, so from the polar form we can see that
    $$|z| = |r(\cos\theta + i\sin\theta)| = |r|\cdot|\cos\theta + i\sin\theta|$$ \pause
    Now, remembering that $|a + bi| = \sqrt{a^2 + b^2}$, we find that
    $$|\cos\theta + i\sin\theta| = \sqrt{\cos^2\theta + \sin^2\theta} = 1,$$
    because $\sin^2x + \cos^2x = 1$. So $|z| = |r|\cdot 1 = r$ as should be, and the $(\cos\theta + i\sin\theta)$ -part corresponds to a point on the \textit{unit circle}, defining the direction of the vector.
\end{frame}


\begin{frame}{Unit circle}
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{unit_circle.png}
    \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item All complex numbers on the unit circle have a magnitude of one, $|z| = r = 1$
        \item So for all numbers $z_1$ on the unit circle, $z_1 = \cos\theta + i\sin\theta$
        \pause
        \item Let's try, what value on the unit circle does $\theta = 90^{\circ} = \frac{\pi}{2}$ correspond to? Or $\theta = 180^{\circ} = \pi$?
    \end{itemize}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Euler's formula}
We're moving towards the intuitive interpretation of complex multiplication, but we still can make the calculations easier with one trick, keep with me! It is ``the most remarkable formula in mathematics'' \pause
    \begin{block}{Euler's formula}
    $$ e^{i\theta} = \cos\theta + i\sin\theta,$$
    \end{block}
    where $e = 2,71828 \ldots$ is a mathematical constant also known as Euler's number, $f(z) = e^{z}$ is the \textit{exponential function}, and $\theta$ is always expressed in \textit{radians}
    \pause\\
    A special case of Euler's formula is when $\theta = \pi$, which many consider to be especially beautiful:

    \begin{block}{Euler's identity}
    $$ e^{i\pi} + 1 = 0 $$
    \end{block}
\end{frame}

\begin{frame}{xkcd --- e to the pi times i}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\textwidth]{e_to_the_pi_times_i.png}
    \end{figure}
\end{frame}


\begin{frame}{Multiplication of complex numbers in exponential form}
\begin{itemize}
    \item<1-> Now we can finally express a complex number in polar form as $z = r\cdot e^{i\theta}$
    \item<1-> This is also known as the \textit{exponential} form
    \item<2-> Since we know that $x^a\cdot x^b = x^{a + b},$ we can now write:
    \begin{align*}
    \uncover<2->{z\cdot w &= r_z e^{i\theta_z} \cdot r_w e^{i\theta_w} }\\
    \uncover<3->{ &= (r_z\cdot r_w)(e^{i\theta_z}\cdot e^{i\theta_w}) }\\
    \uncover<4->{ &= r_z r_w e^{i\theta_z + i\theta_w} }\\
    \uncover<5->{ &= r_z r_w e^{i(\theta_z + \theta_w)} }
    \end{align*}
    \item<6-> So we have a new complex number $z_2$, for which $r_{z_2} = r_z r_w$ and $\theta_{z_2} = \theta_z + \theta_w$
    \item<7-> Interpretation: multiplying a complex number increases its length by a factor of the multiplier's magnitude, and turns it by the amount of the multiplier's argument
\end{itemize}
\end{frame}

\begin{frame}{Complex multiplication}
\begin{columns}
    \begin{column}{0.6\textwidth}
        \begin{figure}
            \centering
            \includegraphics<1|handout:0>[width=\textwidth]{complex_mult-1.png}
            \includegraphics<2>[width=\textwidth]{complex_mult-2.png}
        \end{figure}
    \end{column}
    \begin{column}{0.4\textwidth}
    \begin{align*}
        \theta &= \arctan\left(\frac{b}{a}\right) \\
        r &= \sqrt{a^2 + b^2} \\
        z\cdot w &= z_2 = r_2 e^{i\theta_2}, \text{ where} \\
        r_2 &= r_z \cdot r_w \\
        \theta_2 &= \theta_z + \theta_w
    \end{align*}
    \end{column}
\end{columns}
\end{frame}

\subsection{Complex conjugate and division}
\begin{frame}{Complex conjugate}
\begin{columns}[t]
    \begin{column}{0.5\textwidth}
    \begin{block}<1->{Rectangular form}
    For $z = a + bi$, the complex conjugate is $\bar{z} = a - bi$
    \end{block}
    \begin{block}<2->{Exponential form}
    For $z = r e^{i\theta}$, the complex conjugate is $\bar{z} = r e^{-i\theta}$
    \end{block}
    \uncover<3->{The complex conjugate is often written as either $\bar{z}$ or $z^*$}
    \end{column}
    \begin{column}{0.45\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{complex_conjugate.png}
    \end{figure}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Complex conjugate and the magnitude of a complex number}

Multiplying a complex number $z$ by its complex conjugate $\bar{z}$, we get:
\begin{align*}
    z\bar{z} &= (a + bi)(a - bi) = a^2 - abi + bia - bibi \\
    &= a^2 - \cancel{abi} + \cancel{abi} - i^2b^2 \\
    &= a^2 - (-1)b^2 = a^2 + b^2
\end{align*} \pause
Recall that in polar form, $r = \sqrt{a^2 + b^2}$. Comparing to the above, we see that $\sqrt{z\bar{z}} = \sqrt{a^2 + b^2} = r = |z| = |\bar{z}|$ \\\pause

\end{frame}

\begin{frame}{$z\bar{z}$}
\begin{figure}
    \centering
    \includegraphics<1|handout:0>[width=0.8\textwidth]{complex_modulus-1.png}
    \includegraphics<2|handout:0>[width=0.8\textwidth]{complex_modulus-2.png}
    \includegraphics<3>[width=0.8\textwidth]{complex_modulus-3.png}
\end{figure}
\end{frame}

\begin{frame}{Properties of the complex conjugate}
For two complex numbers $z$ and $w$,
\begin{align*}
    \overline{z + w} &= \bar{z} + \bar{w} \\
    \overline{z - w} &= \bar{z} - \bar{w} \\
    \overline{z\cdot w} &= \bar{z}\cdot\bar{w} \\
    \overline{\left(\frac{z}{w}\right)} &= \frac{\bar{z}}{\bar{w}} \\
    \overline{\overline{z}} &= z
\end{align*}
\end{frame}


\begin{frame}{Complex division}
Division is the opposite operation to multiplication, so intuitively we can already have an idea what the result of division is \ldots{} right?\pause\\
In exponential form:
\begin{align*}
    \frac{z}{w} &= \frac{r_z e^{i\theta_z}}{r_w e^{i\theta_w}} 
    = \frac{r_z}{r_w} \frac{e^{i\theta_z}}{e^{i\theta_w}}
    = \frac{r_z}{r_w} e^{i\theta_z}\cdot e^{-i\theta_w}
    = \frac{r_z}{r_w} e^{i\theta_z + (-i\theta_w)} \\
    &= \frac{r_z}{r_w} e^{i(\theta_z - \theta_w)}
\end{align*}
In rectangular form:
\begin{align*}
\frac{z}{w} = \frac{a + bi}{c + di} = \frac{a + bi}{c + di} \frac{c - di}{\underbrace{c - di}_{\bar{w}}} = \frac{(a+bi)(c-di)}{c^2 + d^2}
\end{align*}

\end{frame}


\begin{frame}{Reciprocal}
Using the result from the previous slide, we can express the reciprocal of a complex number $z$ as:
$$z^{-1} = \frac{1}{z} = \frac{1}{a + bi} = \frac{a - bi}{a^2 + b^2} = \frac{a}{a^2 + b^2} - \frac{b}{a^2 + b^2} \cdot i$$
And in exponential form, more straightforwardly:
$$z^{-1} = (r e^{i\theta})^{-1} = r^{-1} e^{i\theta\cdot (-1)} = \frac{1}{r} e^{-i\theta}$$
\end{frame}


\begin{frame}{Properties of complex numbers}
\begin{columns}[t]
    \begin{column}{0.45\textwidth}
    
    \begin{block}{Commutativity}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
        z + w &= w + z \\
        zw &= wz
    \end{align*}    
    \end{block}

    \begin{block}{Associativity}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
    z + (w + v) &= (z + w) + v \\
    z(wv) &= (zw)v
    \end{align*}
    \end{block}

    \begin{block}{Distributive law}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
        z(w + v) &= zw + zv \\
        &= wz + vz = (w + v)z
    \end{align*}
    \end{block}


    \end{column}
    \begin{column}{0.45\textwidth}
    \begin{block}{Equality, exponential form}
    $z = a + bi$ equals $w = c + di$ if and only if $a = b$ \textbf{and} $c = d$
    \end{block}
    \begin{block}{Equality, polar form}
    $z_1 = r_1 e^{i\theta_1}$ equals $z_2 = r_2 e^{i\theta_2}$ if and only if $r_1 = r_2$ \textbf{and} $\theta_1 = \theta_2$
    \end{block}

    \end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]{MATLAB commands for complex numbers}
\begin{columns}[t]
    \begin{column}{0.45\textwidth}
    
    \begin{block}{Define complex variable}
    \begin{verbatim}
z = 3 + 2i;
w = 5.23 - 1.3232j;\end{verbatim}
    \end{block}
    
    \begin{block}{Real \& imaginary parts}
    \begin{verbatim}
a = real(z);
b = imag(z);\end{verbatim}
    \end{block}
    
    \begin{block}{Argument}
    \begin{verbatim}
theta = angle(z);\end{verbatim}
    \end{block}
    
    \begin{block}{Magnitude}
    \begin{verbatim}
r = abs(z);\end{verbatim}
    \end{block}
    
    \end{column}
    
    \begin{column}{0.45\textwidth}
    
    \begin{block}{Complex conjugate}
    \begin{verbatim}
z_conj = conj(z);
z_conj = z';\end{verbatim}
    \end{block}
    
    \begin{block}{$\arctan(x)$}
    \begin{verbatim}
theta = atan(x);\end{verbatim}
    \end{block}
    
    \begin{block}{$e^x$}
    \begin{verbatim}
y = exp(x);
z = r*exp(i*theta);\end{verbatim}
    \end{block}
    
    \end{column}
\end{columns}
\end{frame}

\end{document}
