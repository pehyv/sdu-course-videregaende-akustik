\documentclass[usenames,dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{icomma}
\usepackage{verbatim}
\usepackage{animate}
\usepackage{siunitx}
\usepackage{circuitikz}
\usetheme{AnnArbor}
\usecolortheme{seagull}

\graphicspath{ {lecture_6/} }
\ctikzset{bipoles/length=.8cm,bipoles/thickness=1}

\title{Lecture 6 --- Acoustic impedance and admittance}
\subtitle{Advanced Acoustics / Vidergående Akustik / F19}
\author{Petteri Hyvärinen}
\date{March 18th 2019}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Acoustic impedance}
\subsection{Options for equivalent circuits}
\begin{frame}{Today's program}
    \begin{itemize}
        \item Today, the main goal is to understand how \textbf{tympanometry} works
        \item We'll use the tools from previous lectures but we still need to define acoustic impedance
    \end{itemize}
\end{frame}

\begin{frame}{Intro}
    \begin{itemize}
        \item So far, we've introduced complex numbers, electrical impedance, and electrical--mechanical analogies
        \item Today, we'll look at electrical--acoustic analogies
        \item Are we still sure we're making things simpler and not just harder?
        \begin{itemize}
            \item Or, how are the earlier things helping us at all?
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Wave equation}
    \begin{itemize}
        \item<1-> We could indeed still just work without complex numbers and equivalent circuits
        \item<2-> In principle, all acoustic scenarios could be explained by investigating the so-called \emph{wave equation}:
        $$\frac{\partial^2 \textbf{u}(x, y, z; t)}{\partial t^2} = c^2 \nabla^2 \textbf{u}(x, y, z; t),$$
        where $\nabla$ is a second-order differential operator called a \emph{Laplacian}: $\left(\frac{\partial^2}{\partial x^2}, \frac{\partial^2}{\partial y^2},  \frac{\partial^2}{\partial z^2}\right)$
        \item<3-> Solving this (\emph{i.e.} knowing what value $u$ takes in a point $(x,y,z)$ at the time $t$ is in general quite difficult
        \begin{itemize}
            \item We would for example have to mathematically describe the boundaries of our area of interest (\emph{e.g.} the ear canal) which is in itself not an easy task
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Equivalent circuits}
    \begin{itemize}
        \item<1-> So, instead of solving the wave equation, we make ``some'' (\emph{a lot of}) simplifying assumptions and model our acoustic system as an equivalent circuit
        \item<2-> The equivalent circuit gives us results that are not too far off from the reality
        \item<3-> The results are also easier to understand, based on our knowledge from electrical engineering
    \end{itemize}
\end{frame}

\subsection{Acoustic impedance}
\begin{frame}{Acoustic impedance}
    \begin{itemize}
        \item<1-> Acoustic impedance is defined as:
        \begin{block}{Acoustic impedance}
        $$Z_a = \frac{p}{U},$$
        \end{block}
        where $p$ is pressure and $U$ is volume velocity (particle velocity $\times$ area)
        $$U = u\cdot S,$$ where $S$ is the cross-sectional area across which the particle velocity $u$ is evaluated ($u$ is assumed to be uniform across $S$)
        \item<2-> Compare this to electrical impedance:
        $$Z_e = \frac{V}{I}$$
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic impedance}
    \begin{itemize}
        \item<1-> In the impedance analogy, we treat pressure as analogous to voltage, and volume velocity as analogous to current
        
        \item<2-> This means, that when we calculate a component $A$ in the equivalent electrical circuit to have \textbf{current} $I_A$ flowing through it, we interpret it to mean that in the \emph{acoustic domain} the acoustic equivalent of $A$ is \textbf{moving} at speed $u = I_A$
        
        \item<3-> Likewise, if a component $A$ in the equivalent electrical circuit has a \textbf{voltage} $V_A$ across it, we interpret it to mean that in the acoustic domain the acoustic equivalent of $A$ experiences a \textbf{force} $F = V_A$
    \end{itemize}
\end{frame}

\subsection{Acoustic elements}
\begin{frame}{Acoustic elements and their impedances}
    \begin{itemize}
        \item<1-> We will now go through the three basic acoustic components
        \item<2-> Again, we are considering the system's response to harmonic motion. This means that the \textbf{volume velocity} will be of the form: $$U(t) = u_p\cdot S \cdot \cos(\omega t)$$
        \item<3-> As before, \textbf{displacement} is related to particle velocity
        $$\xi(t) = \int u(t) \,dt =  \frac{u_p}{\omega}\cdot sin(\omega t) = \frac{u_p}{\omega}\cos(\omega t - \pi/2)$$
        \item<4-> and \textbf{acceleration}:
        $$a(t) = \dot{u} = \frac{\partial u}{\partial t} = -\omega\cdot u_p \cdot \sin(\omega t) = \omega \cdot u_p \cdot \cos(\omega t + \pi/2)$$
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic elements and their impedances}
    \begin{itemize}
        \item<1-> And similarly, if we express $U(t)$ as a complex phasor we get:
        $$U(t) = u_p \cdot S \cdot e^{i\omega t}$$
        \item<2-> For \textbf{displacement}:
        $$\xi(t) = \int u(t) \,dt =  u_p \int e^{i\omega t} \, dt = \frac{u_p}{i\omega}\cdot e^{i\omega t}$$
        \item<3-> and for \textbf{acceleration}:
        $$a(t) = \dot{u} = \frac{\partial u}{\partial t} = i\omega\cdot u_p \cdot e^{i\omega t}$$
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic compliance}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{acoustic_compliance.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{block}{Acoustic compliance}        \setlength\abovedisplayskip{0pt}
    \begin{align*}
        C_a &= \frac{\xi S}{p} \\
        \Leftrightarrow p &= \frac{\xi S}{C_a}
    \end{align*}
    
    \end{block}
    \end{column}
    \end{columns}
    \pause
    Now, substituting the result for $p$ from above, and expressing $\xi$ and $U$ in the phasor form:
    \begin{align*}
        Z_a &= \frac{p}{U} = \frac{\frac{\xi S}{C_a}}{U} = \frac{\frac{u_p}{i\omega}\cdot \cancel{e^{i\omega t}}\cdot S}{C_a\cdot U_p \cdot \cancel{e^{i\omega t}}} = \frac{\overbrace{u_p\cdot S}^{U_p}}{i\omega\cdot C_a \cdot U_p} = \frac{\cancel{U_p}}{i\omega\cdot C_a \cdot \cancel{U_p}} \\
        &= \frac{1}{i \omega C_a} = -i\frac{1}{\omega C_a}
    \end{align*}
\end{frame}

\begin{frame}{Compliance of a volume of air}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{acoustic_compliance_volume.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{block}{Compliance of volume $V$}
    $$C_a = \frac{V}{\gamma P} = \frac{V}{\rho_0 c^2}$$
    \end{block}
    \end{column}
    \end{columns}
    \pause
    Using the formula from previous slide, the acoustic impedance of a volume of air is:
    $$Z_a = -i \frac{1}{\omega C_a} = -i \frac{1}{\omega \frac{V}{\rho_0 c^2}} = -i \frac{\rho_o c^2}{\omega V}$$
\end{frame}

\begin{frame}{Acoustic mass}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{acoustic_inertance.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{block}{Acoustic mass (inertance)}
    \begin{align*}
        M_a &= \frac{m}{S^2} = \frac{p}{a\cdot S} \\
        \Leftrightarrow p &= M_a \cdot a \cdot S
    \end{align*}
    
    \end{block}
    \end{column}
    \end{columns}
    \pause
    Substituting the result for $p$ from above, and expressing $a$ and $U$ in phasor form:
    \begin{align*}
        Z_a &= \frac{p}{U} = \frac{M_a\cdot a \cdot S}{U} = \frac{M_a \cdot u_p \cdot i\omega \cdot \cancel{e^{i\omega t}}\cdot S}{U_p \cdot \cancel{e^{i\omega t}}} = \frac{M_a \cdot i\omega \cdot \overbrace{u_p\cdot S}^{U_p}}{U_p} \\
        &= \frac{i\omega \cdot M_a \cdot \cancel{U_p}}{\cancel{U_p}} = i \omega M_a
    \end{align*}
\end{frame}

\begin{frame}{Acoustic mass of an air column}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ac_mass_crop.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{block}{Mass of air in a tube}
    $$M_a = \frac{m}{S^2} = \frac{\rho_0 \cdot L\cdot S}{S^2} = \frac{\rho_0 \cdot L}{S}$$
    \end{block}
    \end{column}
    \end{columns}
    \pause
    Using the formula from previous slide, the acoustic impedance of the mass is:
    $$Z_a = i \omega M_a = i \frac{\omega \rho_0 L}{S}$$
    \pause
    \textbf{Bemærk!} For a finite tube, the air outside the end of the tube is also moving. Therefore, you always need to add an \emph{end correction} value to the actual length $l$ to get an \textbf{effective length} $L$
\end{frame}

\begin{frame}{Acoustic resistance}
    \begin{columns}
    \begin{column}{0.35\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{ac_resistance.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.6\textwidth}
    \begin{itemize}
        \item<1-> Acoustic resistance absorbs sound (sound waves are turned into heat)
        \begin{itemize}
            \item For example, fibreglass or thick fabric such as felt
            \item Soundproofing, sound attenuation
        \end{itemize}
        \item<2-> Values for acoustic resistance are usually found experimentally -- not by theoretical calculations
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Acoustic impedance, summary}
    \begin{itemize}
        \item<1-> As before, we can split the acoustic impedance into its real and imaginary parts to get acoustic \textbf{resistance} and acoustic \textbf{reactance}:
        $$Z_a = R_a + i(X_C + X_M) = R_a - i\frac{1}{\omega C_a} + i\omega M_a$$
        \item<2-> Comparing the different terms of acoustic impedance with the electrical impedance, we see again a correspondence between acoustic and electrical components
    \end{itemize}
\end{frame}

\subsection{Equivalent circuits of acoustic systems}
\ctikzset{bipoles/length=1cm,bipoles/thickness=1}
\begin{frame}{Electrical, mechanical \& acoustic analogies \\(impedance analogy)}
    \begin{center}
        \begin{tabular}{ c c c c c }
         \textbf{Mechanical} & & \textbf{Electrical} & & \textbf{Acoustic}\\ 
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[mass=$m$ ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[cute inductor=$L$ ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         $M_a$
         \\
         Mass & & Inductance & & Acoustic mass (inertance)\\
         & &\\
         
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[spring=\mbox{$C_m=\frac{1}{s}$} ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[C=$C$ ] (2,0); \end{circuitikz} 
         &
         $\longleftrightarrow$
         &
         $C_a$
         \\
         Compliance & & Capacitance & & Acoustic compliance \\
         & &\\
         
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[damper=$R_M$ ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[european resistor=$R$ ] (2,0); \end{circuitikz} 
         &
         $\longleftrightarrow$
         &
         $R_a$
         \\
         Resistance & & Resistance & & Acoustic resistance
        \end{tabular}
    \end{center}
\end{frame}

\subsection{Helmholtz resonator}
\begin{frame}{Helmholtz resonator}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{helmholz_bottle_cropped.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{itemize}
        \item<1-> A \textbf{Helmholtz resonator} is the acoustic equivalent of a mechanical or electrical resonating system
        \item<2> If you take a bottle and blow on the mouth, the frequency of the sound is the resonance frequency of the system
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Helmholtz resonator}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{helmholz_bottle_cropped.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{itemize}
        \item<1-> The inertance comes from the acoustic mass $M_a$ of the air column in the neck of the bottle
        \item<2-> The compliance $C_a$ is due to the air volume inside the bottle
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Helmholtz resonator -- equivalent circuit}
    \begin{columns}
    \begin{column}{0.3\textwidth}
        \centering
        \begin{circuitikz}[line width=1pt]
        \ctikzset{bipoles/length=.8cm,bipoles/thickness=1}
        \draw (0,3) to[short, *-]
        (1,3) to[european resistor, l=\mbox{$R = R_a$}] (1,2)
        to[C, l=\mbox{$C = C_a$}] (1,1)
        to[cute inductor, l=\mbox{$L=M_a$}] (1,0)
        to[short, -*] (0,0);
        \end{circuitikz}
    \end{column}
    
    \begin{column}{0.6\textwidth}
    The total impedance of the system is:
    \begin{align*}
        Z_a &= R_a - i\frac{1}{\omega C_a} + i\omega M_a \\
        &= R_a - i \frac{\rho_0 c^2}{\omega V} + i \frac{\omega \rho_0 L}{S}
    \end{align*}
    \pause
    At the resonance frequency $f_0 = \omega_0/2\pi$, imaginary part of impedance is zero:
    $$-i\frac{1}{\omega_0 C_a} + i\omega_0 M_a = 0$$\pause
    $$\Leftrightarrow \omega_0 = \sqrt{\frac{1}{M_a C_a}} = \sqrt{\frac{1}{\frac{\rho_0 L}{S} \frac{V}{\rho_0 c^2}}} = c\cdot\sqrt{\frac{S}{L V}}$$
    \end{column}
    \end{columns}
\end{frame}

\subsection{Acoustic admittance}
\begin{frame}{Acoustic admittance}
    \begin{itemize}
        \item<1-> Admittance is the reciprocal of impedance:
        $$Y_a = \frac{1}{Z_a} = \frac{U}{p}$$
        \item<2-> Just like impedance $Z$ can be split into two parts: resistance $R$ and reactance $X$ \ldots
        $$Z = R + iX$$
        \item<3-> \ldots admittance $Y$ also has two parts: \textbf{conductance} $G$ and \\ \textbf{susceptance} $B$
        $$Y = G + iB$$
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic admittance}
    \begin{itemize}
        \item Admittance is used especially in audiology, since the ear appears as an acoustic system which has multiple impedances connected in \emph{parallel}
        \begin{figure}
            \centering
            \includegraphics[width=0.6\textwidth]{ear_equivalent_circuit.png}
        \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic admittance -- parallel configuration}
    \begin{itemize}
        \item The total impedance of a system with impedances $Z_1, Z_2, \ldots Z_n$ connected in parallel is determined by:
        $$\frac{1}{Z} = \frac{1}{Z_1} + \frac{1}{Z_2} + \ldots + \frac{1}{Z_n}$$
        \item Switching to admittance simplifies calculations for these kind of systems:
        $$Y = Y_1 + Y_2 + \ldots + Y_n$$
    \end{itemize}
\end{frame}

\subsection{Units of acoustic impedance}
\begin{frame}{Units of acoustic impedance}
    \begin{itemize}
        \item<1-> The international system of units (\emph{SI-system}) uses the \textbf{MKS} units
        \begin{itemize}
            \item MKS: meters--kilograms--seconds
            \item As opposed to the old CGS-system: centimeters--grams--seconds
        \end{itemize}
        \item<2-> The unit for \emph{acoustic impedance} is \si{Pa.s/m^3} = \si{N.s/m^5} = \si{kg / m^4.s}
        \begin{itemize}
            \item The term \textbf{acoustic ohm} refers to the old CGS-system. In MKS units, 1 acoustic ohm = \SI{1e5}{Pa.s/m^3}
            \item Sometimes, \SI{1}{Pa.s/m^3} is called \emph{MKS acoustic ohm}
            \item Sometimes when referring to acoustic admittance the unit \textbf{mho} is used. 1 acoustic mho = \SI{1e-5}{m^3/Pa.s} = \SI{1e-5}{m^5/N.s} = \SI{1e-5}{m^4.s/kg}
        \end{itemize}
        \item<3-> The unit for \emph{acoustic inertance} is \si{Pa.s^2 / m^3} = \si{N.s^2/m^5} = \si{kg / m^4}
        \begin{itemize}
            \item When used in equivalent circuits with acoustic ohms,\\ 1 acoustic henry = \SI{1e5}{Pa.s^2 / m^3}
        \end{itemize}
        \item<4-> The unit for \emph{acoustic compliance} is \si{m^3 / Pa} = \si{m^5/N} = \si{m^4.s^2 / kg}
        \begin{itemize}
            \item When used in equivalent circuits with acoustic ohms,\\ 1 acoustic farad = \SI{1e-5}{m^3 / Pa}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Equivalent air volume}
    \begin{itemize}
        \item<1-> In audiology, equivalent air volume is often used as a unit of acoustic impedance
        \item<2-> Equivalent volume is the volume of a cavity that would have the same magnitude of impedance as  an acoustic impedance $Z_a = R_a + i(X_C + X_M)$ containing both resistive and reactive components 
        \item<3-> The acoustic impedance of a volume of air is
        $-i\frac{\rho_o c^2}{\omega V}$ and its magnitude $\frac{\rho_0 c^2}{\omega V}$
        \item<4-> From here, we get the equivalent volume $V_e$ of an impedance $Z_a$:
        $$|Z_a| = \frac{\rho_0 c^2}{\omega V_e} \quad \Leftrightarrow \quad V_e = \frac{\rho_0 c^2}{\omega |Z_a|}$$
        \item<5-> At 226 Hz, the impedance of a volume of \SI{1}{cm^2} has a magnitude of $\frac{\SI{1,1839}{kg/m^3} \cdot (\SI{346,13}{m/s})^2}{2\pi\cdot \SI{226}{Hz}\cdot \SI{1e-6}{m^3}} \approx \SI{99886132}{Pa.s/m^3} \approx \SI{1}{k\Omega} \Leftrightarrow \SI{1}{mmho}$
    \end{itemize}
\end{frame}

\subsection{Tympanometry}
\begin{frame}{Tympanometry}
    \begin{itemize}
        \item<1-> Tympanometry is a traditional objective measure of middle ear function
        \item<2-> There the admittance of the middle ear is measured as a function of static air pressure in the ear canal
        \item<3-> The idea is that the transmission of sound through the eardrum and middle ear is most efficient when there is no pressure difference between the middle ear and ear canal
        \item<4-> When there is over- or underpressure (relative to middle ear cavity) in the ear canal, the eardrum and ossicular chain are stiffer and therefore the admittance is poorer (smaller in value)
    \end{itemize}
\end{frame}

\begin{frame}{Tympanometry}
    \begin{itemize}
        \item<1-> The measurement is done at the probe, which is in the ear canal entrance
        \item<2-> Therefore, both the ear canal and the middle ear admittances (in parallel) are measured
        \begin{itemize}
            \item However, the ear canal admittance does not change with static pressure, so it is possible to factor it out
        \end{itemize}
    \end{itemize}
        \begin{figure}
            \centering
            \includegraphics[width=0.6\textwidth]{tympanometry_circuit.png}
        \end{figure}
\end{frame}

\begin{frame}{Ear canal + middle ear}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ear_canal_middle_ear.png}
    \end{figure}
\end{frame}

\begin{frame}{Tympanogram}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{tympanogram.png}
    \end{figure}
\end{frame}

\subsection{Immittance measures}
\begin{frame}{Other immittance measures}
    \begin{itemize}
        \item<1-> Traditional, old-school tympanometry is a good tool, but only gives information at a single frequency
        \begin{itemize}
            \item Multi-frequency tympanometry can be performed using more than one frequency
        \end{itemize}
        \item<2-> Wideband acoustic immittance (WAI) uses broadband stimuli (\emph{e.g.} clicks) to measure the impedance of the middle ear across a wide range of frequencies
        \item<3-> Wideband tympanometry (WBT) adds the effect of static ear canal pressure to WAI
    \end{itemize}
\end{frame}

\begin{frame}{WBT}
    \centering
    \begin{figure}
        \centering
        \includegraphics[width=0.75\textwidth]{wbt_3d.png}
    \end{figure}
    \tiny Source: Interacoustics Titan Manual
\end{frame}

\subsection{Specific acoustic impedance}
\begin{frame}{Specific acoustic impedance}
    \begin{itemize}
        \item<1-> The acoustic impedance we have been discussing so far is useful when dealing with structures that emit or receive sound
        \begin{itemize}
            \item \emph{e.g.} loudspeakers, microphones, the ear
        \end{itemize}
        \item<2-> However, another type of acoustic impedance, called the \textbf{specific acoustic impedance} relates to how sound propagates in a medium
        \begin{block}{Specific acoustic impedance}
        $$Z = \frac{p}{u},$$
        \end{block}
        where $p$ is sound pressure and $u$ is the particle velocity
        \item<3-> The unit of specific acoustic impedance is \si{N.s/m^3} (vs. \si{N.s/m^5} for acoustic impedance before!)
    \end{itemize}
\end{frame}

\begin{frame}{Characteristic specific acoustic impedance}
    \begin{itemize}
        \item<1-> In general, specific acoustic impedance is a complex number
        \item<2-> But for a plane wave, the specific acoustic impedance has the following purely real value
        \begin{block}{Characteristic specific acoustic impedance}
        $$Z = \rho_0 \cdot c,$$
        \end{block}
        where $\rho_0$ is the density of the medium, and $c$ is the speed of sound
        \item<3-> The characteristic impedance tells how fast a sound wave decays in the medium
    \end{itemize}
\end{frame}

\subsection{Next week}
\begin{frame}{Next week}
\begin{itemize}
    \item Next week we will take a look at the first lab exercise
    \item The exercise itself will take place on \textbf{Wednesday 27/3/2019, 12--14} in the audiology lab!
\end{itemize}
\end{frame}

\end{document}
