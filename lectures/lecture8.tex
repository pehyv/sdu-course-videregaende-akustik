\documentclass[usenames,dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{icomma}
\usepackage{verbatim}
\usepackage{animate}
\usepackage{siunitx}
\usepackage{circuitikz}
\usetheme{AnnArbor}
\usecolortheme{seagull}

\graphicspath{ {lecture_8/} }
\ctikzset{bipoles/length=.8cm,bipoles/thickness=1}

\title{Lecture 8 --- Audiometric test rooms}
\subtitle{Advanced Acoustics / Vidergående Akustik / F19}
\author{Petteri Hyvärinen}
\date{April 29th 2019}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Masking}
\subsection{Simultaneous masking}
\begin{frame}{Multiple sound sources}
    \begin{itemize}
        \item<1-> Most of us have probably experienced a situation where we did not notice that our mobile phone was ringing even though the volume had been set to a moderate level
        \begin{itemize}
            \item<2-> Most likely this happened in a noisy environment?
        \end{itemize}
        \item<3-> This is an example of auditory \emph{masking}
        \begin{itemize}
            \item<4-> \emph{i.e.} when the presence one sound makes it harder to hear another sound
            \item<4-> Note, that in a silent situation, we would definitely hear the phone ringing, but we might also hear the phone in noise if we set the ringer to maximum volume
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Auditory masking}
\begin{columns}
\begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{masking_curves.png}
    \end{figure}
\end{column}

\begin{column}{0.45\textwidth}
    \begin{itemize}
        \item A narrow band of noise is presented at 1200 Hz
        \item Depending on the level of the noise (from 20 to 110 dB SPL), the hearing thresholds for tones around the noise band are elevated
    \end{itemize}
\end{column}
\end{columns}
\end{frame}

\subsection{Basilar membrane}
\begin{frame}{Basilar membrane}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{bm_amplitude.png}
    \end{figure}
    \begin{itemize}
        \item<1->  Compare the masking curves to basilar membrane movement for different frequencies (high frequencies closer to stapes)
    \end{itemize}
\end{frame}

\subsection{Critical bands}
\begin{frame}{Critical bands}
    \begin{itemize}
        \item<1-> When the bandwidth of the masker is increased, at threshold increases until the masker bandwidth reaches the so-called \textbf{critical bandwidth}
    \end{itemize}
    
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics<1>[width=\textwidth]{target_masker.png}
        \includegraphics<2>[width=\textwidth]{target_masker_1.png}
        \includegraphics<3>[width=\textwidth]{target_masker_2.png}
        \includegraphics<4>[width=\textwidth]{target_masker_3.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{critical_band_thresholds.PNG}
        \end{figure}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Critical bands}
    \begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{critical_band_table.png}
        \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<1-> Critical bands are just \emph{approximating} the behaviour of auditory filters
            \item<2-> When masking sound is presented \emph{within} the critical band, we call it \textbf{local masking}
            \item<3-> When masking sound is presented \emph{outside} the critical band, we call it \textbf{remote masking}
            \item<4-> \textbf{Bemærk!} Auditory filters (and critical bands) get \emph{broader} in hearing loss!
        \end{itemize}
    \end{column}
    \end{columns}
\end{frame}


\section{Audiometric rooms}
\subsection{Audiometry}
\begin{frame}{Audiometry}
    \begin{itemize}
        \item<1-> In audiometry, we try to find the lowest level of a sound that a person can hear
        \item<2-> Very low-level sounds can be masked by low-level noise
        \item<3-> $\Rightarrow$ the audiometric environment must have a very low background noise level
        \item<4-> But it is enough to consider only \emph{local masking} --- why?
        \item<5-> In audiometry, background noise affects the results of normal-hearing subjects more than hearing-impaired --- why?
    \end{itemize}
\end{frame}


\begin{frame}{Audiometry}
    \begin{columns}
    \begin{column}{0.45\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=0.9\textwidth]{bg_noise_levels.png}
        \end{figure}
    \end{column}
    
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<1-> Here the maximum noise levels are given when measuring down to 0 dB HL at 250 Hz -- 8000 Hz
            \item<2-> If the lowest level should be less, the background noise also has to be lower by the same amount
            \begin{itemize}
                \item<3-> \emph{i.e.} $-$10 dB HL $\Rightarrow$ max noise level at 1 kHz $=$ 13 dB SPL
            \end{itemize}
        \end{itemize}
    \end{column}
    \end{columns}
\end{frame}


\begin{frame}{Audiometry}
    \begin{itemize}
        \item<1-> Note that the maximum permissible noise levels are \emph{above} diffuse-field hearing thresholds
        \begin{itemize}
            \item<2-> Doesn't the room need to be \emph{quieter} than the target sounds?
            \item<3-> Keep in mind that the noise levels are measured with 1/3-octave bands, whereas the auditory filters are narrower than that! $\Rightarrow$ less noise is ``picked up'' by the auditory system than what is measured with a SLM
        \end{itemize}
        \item<4-> So, be careful when comparing tone thresholds and noise thresholds --- the book is a bit unclear in this section
        \item<5-> Also, remember that regular SLMs cannot measure below 10 -- 20 dB SPL $\Rightarrow$ special equipment needed for verifying audiometric test rooms
    \end{itemize}
    
\end{frame}


\section{Room acoustics}
\begin{frame}{Room acoustics}
    \begin{itemize}
        \item<1-> So now we know what the noise limits are, but how do we achieve those conditions?
        \item<2-> The acoustics of the test room have to be controlled
        \begin{itemize}
            \item<3-> Isolation from external sounds
            \item<4-> Control of internal sounds
        \end{itemize}
        \item<5-> Now we focus more on the internal sound
        \begin{itemize}
            \item<6-> An important application is sound field audiometry -- we have to know how the sound behaves on the way from the loudspeaker to the ear
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Free field}
\begin{frame}{Free field}
    \begin{itemize}
        \item<1-> If we could place a sound source in the middle of an empty space, away from any surfaces, the sound pressure measured at distance $r$ from the source would be proportional to $\frac{1}{r}$
        \item<2-> Any environment where the sound pressure follows the same behaviour, \emph{i.e.} is proportional to $\frac{1}{r}$, is called a \textbf{free field} and we only measure the direct sound from the source
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=0.35\textwidth]{freefield.png}
    \end{figure}
\end{frame}


\begin{frame}{Free field}
    \begin{itemize}
        \item<1-> In practice, when we have walls, floor, ceiling, close to the sound source, part of the sound is reflected and the reflections add together with the direct sound, and the sound pressure is not anymore proportional to $\frac{1}{r}$ because of these reflections
        \begin{itemize}
            \item \emph{Anechoic chambers} are an exception, because (almost) no reflections occur there
        \end{itemize}
    \end{itemize}
        \begin{figure}
        \centering
        \includegraphics[width=0.35\textwidth]{reflections.png}
    \end{figure}
\end{frame}


\subsection{Reverberation}
\begin{frame}{Reverberation}
    \begin{itemize}
        \item<1-> When a soundwave hits a surface, part of it is reflected
        \item<2-> In a room, sound keeps reflecting from the walls, floor, and ceiling and bounces around in the room
        \item<3-> This results in reverberation, which means that a sound field exists even after a sound source is turned off
        \begin{itemize}
            \item<4-> The reverberating sound decays to silence due to absorption at the reflecting surfaces and by the air in the room
            \item<5-> The time it takes for the sound to fade down by 60 dB is called \textbf{reverberation time}
        \end{itemize}
        \item<6-> The direct sound and first reflections arrive from specific directions
        \begin{itemize}
            \item The reverberation sound field is \textbf{diffuse}, meaning that sound arrives from all directions
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Reverberation time}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{reverberation_time.png}
    \end{figure}
\end{frame}

\begin{frame}{Controlling reverberation}
    \begin{itemize}
        \item<1-> The sound energy stored in a reverberant sound field in a room is proportional to the volume of the room
        \item<2-> The rate of reverberation decay depends on the area of all surfaces in the room and on their ability to absorb sound
        \begin{itemize}
            \item<3-> More absorption means less reflected sound $\Rightarrow$ faster decay, shorter reverberation time
            \item<4-> On the other hand, hard surfaces with little absorption lead to more reverberation: \emph{e.g.} swimming halls, churches
        \end{itemize}
        \item<5-> These principles are combined in \textbf{Sabine's formula}, which gives an estimate of the reverberation time:
        \begin{block}{Sabine's formula}
        $$RT = T_{60} = 0,161 \cdot \frac{V}{A},$$
        \end{block}
        where $V$ is the volume of the room, and $A$ is the absorption from all surfaces
    \end{itemize}
\end{frame}

\begin{frame}{Sabine's formula}
    \begin{itemize}
        \item<1-> A piece of surface having an area of $S$ and an absorption coefficient $\alpha$ has an absorption of $A = S\cdot \alpha$
        \item<2-> An ideal absorbing surface has an absorption coefficient of $\alpha = 1$, which means that all sound that no sound is reflected
        \begin{itemize}
            \item<3-> An hole in the wall (\emph{e.g.} an open window) is a perfect absorber
        \end{itemize}
        \item<4-> A surface with an absorption coefficient of $\alpha = 0$ does not absorb sound at all --- everything is reflected from the surface
        \begin{itemize}
            \item<5-> In practice, very hard surfaces like stone or concrete can have very low absorption ($\approx$ 0,01--0,05), but not zero
        \end{itemize}
        \item<6-> The unit of $A$ is square meters, and the value corresponds to the equivalent area of ideal absorber that would result in the same absorption
        \begin{itemize}
            \item<7-> So, two square meters of material with $a = 0.5$ would have absorption $A = 2 \,\text{m}^2 \cdot 0.5 = 1\,\text{m}^2$, corresponding to a one square meter hole in the wall
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Sabine's formula}
    \begin{itemize}
        \item<1-> For a room with various different surfaces with differing absorbtion coefficients, the $A$ in Sabine's formula is calculated by adding up all the surfaces:
        $$A = S_1 \alpha_1 + S_2 \alpha_2 + S_3 \alpha_3 + \ldots$$
        \item<2-> The absorption coefficient is frequency-dependent
        \begin{itemize}
            \item<3-> Therefore, also reverberation time is different at different frequencies
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Example}
    As an example, what is the reverberation time at 2 kHz of a 3 m $\times$ 4 m $\times$ 2,5 m (l $\times$ w $\times$ h) room, where the walls are covered with ordinary plaster, the floor is wooden, and the ceiling has acoustic panels?
    
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{absorption_coeffs.png}
    \end{figure}
\end{frame}

\begin{frame}{Non-diffuse situations}
    \begin{itemize}
        \item<1-> Sabine's formula assumes that the sound field in the room is \emph{diffuse}
        \item<2-> Often this is not a valid assumption
        \begin{itemize}
            \item<3-> \emph{e.g.} in a listening booth
        \end{itemize}
        \item<4-> In these cases the reverberation time is calculated with Eyring's formula:
        \begin{block}{Eyring's formula}
        $$T_{60} = 0,161\cdot \frac{V}{-S\cdot \ln(1 - \bar{\alpha})} \quad,$$
        \end{block}
        where $\bar{\alpha}$ is the \textbf{average absorption coefficient}:
        $$\bar{\alpha} = \frac{S_1\alpha_1 + S_2\alpha_2 + S_3\alpha_3 + \ldots}{S_1 + S_2 + S_3 + \ldots}$$
    \end{itemize}
\end{frame}

\subsection{Audiometric rooms}
\begin{frame}{Sound field audiometry}
    \begin{itemize}
        \item<1-> The ISO standard 8253-2 specifies the conditions for sound field audiometry
        \item<2-> The requirements do not directly mention sound absorption or reverberation times
        \begin{itemize}
            \item<3-> Instead, it states three different situations: free field, diffuse field, and quasi-free sound field
            \item<4-> The only possible practical situation in a clinic is the third situation, the quasi-free sound field
            \item<5-> For example, when moving the microphone 10 cm towards or away from the loudspeaker from the listener position, the sound pressure level must not deviate from the $\frac{1}{r}$--rule by more than $\pm$ 1 dB
            \item<6-> This effectively places constraints on the sound-treatment of the room, since reverberation moves the sound field more away from the free field situation
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Next week}
\begin{frame}{Next week}
    \begin{itemize}
        \item Lab exercise 2 in the anechoic chamber (6.5. at 12:00)
        \item Read pages 188--206 from Chapter 6 as preparation for the exercise
        \item For next lecture (13.5.), do exercises 9.3, 9.4, 9.6, 9.7, and 9.8
        \begin{itemize}
            \item In 9.7, use the same absorption coefficient for the floor and ceiling
        \end{itemize}
        \item In the last two lectures (13.5. \& 20.5.) we will recap the topics of the course
        \begin{itemize}
            \item What would you like to focus on?
        \end{itemize}
        \item The exam will be on \textbf{Wednesday 12.6.}
    \end{itemize}
\end{frame}

\end{document} 