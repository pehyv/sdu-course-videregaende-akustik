\documentclass[usenames,dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{icomma}
\usepackage{verbatim}
\usepackage{animate}
\usepackage{siunitx}
\usepackage{circuitikz}
\usetheme{AnnArbor}
\usecolortheme{seagull}

\graphicspath{ {lecture_7/} }
\ctikzset{bipoles/length=.8cm,bipoles/thickness=1}

\title{Lecture 7 --- Distributed acoustic impedance}
\subtitle{Advanced Acoustics / Vidergående Akustik / F19}
\author{Petteri Hyvärinen}
\date{March 25th 2019}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Lab exercise}
\begin{frame}{Lab exercise 1}
\begin{columns}
\begin{column}{0.45\textwidth}
    \begin{itemize}
        \item The vent, together with the residual ear canal volume can be viewed as a helmholtz resonator with a resonance frequency:
        $$f_0 = \frac{c}{2\pi} \sqrt{\frac{S}{L\cdot V}}$$
        \begin{itemize}
            \item $S$ is the cross-sectional area of the vent
            \item $L$ is the effective length of the vent
            \item $V$ is the residual volume
        \end{itemize}
    \end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{earmold.png}
    \end{figure}
\end{column}

\end{columns}
\end{frame}

\begin{frame}{Opgave 1.3}
For a vent with a circular cross-section, we have $S = \pi\cdot r^2 = \pi \cdot (d/2)^2$, so
    \begin{align*}
        f_0 &= \frac{c}{2\pi} \sqrt{\frac{S}{L\cdot V}} = \frac{c}{2\pi}\sqrt{\frac{\pi (d/2)^2}{L V}}
    \end{align*}
    Now we will use two identities that hold for the square root operation: $\sqrt{x^2} = |x|$, and $\sqrt{x\cdot y} = \sqrt{x}\sqrt{y}$. The latter works only if both $x$ and $y$ are positive real numbers. One last trick is to remember that $\frac{a/b}{c} = \frac{a}{b\cdot c}$ From here we get:
    \begin{align*}
        f_0 &= \frac{c}{2\pi}\frac{\sqrt{\pi} \sqrt{(d/2)^2}}{\sqrt{L V}} = \frac{c}{2\pi}\frac{\sqrt{\pi} \cdot d/2}{\sqrt{L V}} = \frac{c}{2\pi}\frac{\sqrt{\pi}\cdot d}{2\cdot \sqrt{L V}} = \frac{c \sqrt{\pi}}{4\pi\cdot}\frac{d}{\sqrt{L V}}
    \end{align*}
    Finally, $\frac{\sqrt{\pi}}{2\pi} = \frac{1}{4\sqrt{\pi}} \approx \frac{1}{7} \Rightarrow f_0 \approx \frac{c}{7}\frac{d}{\sqrt{L V}}$
\end{frame}

\begin{frame}{Opgave 1.4}
    \begin{tabular}{c|c|c|c|c|c|c}
        & Prop A & Prop B & Prop C & Prop D & Prop E & Prop F \\
        længde [mm] & 15 & 15 & 15 & 20 & 20 & 20  \\
        længde [m] & 0.015 & 0.015 & 0.015 & 0.02 & 0.02 & 0.02  \\
        diameter [mm] & 1 & 2 & 3 & 1 & 2 & 3 \\
        diameter [m] & 0.001 & 0.002 & 0.003 & 0.001 & 0.002 & 0.003 \\
        $f_0 =$ [Hz] & 277 & 553 & 830 & 240 & 480 & 719 \\
        $f_0 \approx$ [Hz] & 280 & 561 & 841 & 243 & 486 & 729
    \end{tabular}
\end{frame}

\begin{frame}{Opgave 3.3}
\textbf{Possible sources of error:}
    \begin{itemize}
        \item Inaccurate measurement of the vent length and diameter
        \item Inaccurate positioning of the mold \& coupler in the test box
        \item Inaccurate positioning of the reference microphone in the test box
        \item Leaking of the putty seam (\textit{hæftemasse}) between earmold and coupler
        \item Incorrect mounting of the mold (closing the vent / not closing the sound bore)
        \item Calibration of the measurement system
        \item Rounding errors / mistakes in calculations
        \item Speed of sound (depends on air temperature and pressure)
    \end{itemize}
\end{frame}

\section{Distributed impedance}
\begin{frame}{Distributed acoustic impedance}
    \begin{itemize}
        \item<1-> So far, we have only considered elements which are very small compared to the wavelength of the signals
        \item<2-> The acoustic--electrical equivalent circuits only work at relatively low frequencies
        \item<3-> For example, the length of the ear canal is about \SI{25}{mm}. This corresponds to the full wavelength of a 13,7-kHz tone, or the quarter ($\frac{1}{4}$) wavelength of a 3430-Hz tone
        \item<4-> This means that already at mid-frequencies the ear canal cannot be considered as a single lumped element
        \item<5-> Instead, the impedance is spread, or \emph{distributed} along the structure
    \end{itemize}
\end{frame}

\subsection{Acoustic waveguide}
\begin{frame}{Distributed acoustic impedance}
    \begin{itemize}
        \item<1-> An acoustic waveguide is a structure, where soundwaves travel as plane waves along a single dimension
        \item<2-> \emph{i.e.} a tube or a pipe, where the diameter is small enough (but not too small to introduce remarkable acoustic resistance)
        \item<3-> The impedance of an acoustic waveguide with cross-sectional area $S$ is:
        \begin{block}{Acoustic impedance of a pipe}
        $$Z = \frac{\rho_0 c}{S},$$
        \end{block}
        where $\rho_0 c$ is the characteristic specific impedance of the medium
        \item<3-> This formula only works for an infinite pipe!
    \end{itemize}
\end{frame}

\subsection{Finite pipe}
\begin{frame}{Acoustic impedance of a finite pipe}
    \begin{itemize}
        \item<1-> For a pipe of finite length, the impedance depends on the \emph{boundary conditions} of the pipe
        \begin{itemize}
            \item Boundary conditions mean how the pipe ends look like
            \item \emph{i.e.} is the pipe open at both ends, or is one of them closed? Is there sound-absorbing material in one end of the pipe, \emph{etc.}
        \end{itemize}
        \item<2-> Impedance also depends on the place along the pipe where we are measuring the impedance
        \item<3-> For example, for a pipe with one open end an one closed end (open--closed), length $l$ and cross-sectional area $S$, the impedance \emph{at the open end} is:
        $$Z_o = -i \frac{\rho_0 c}{S}\cot(kl)$$
    \end{itemize}
\end{frame}

\begin{frame}{Standing waves in a finite pipe}
\begin{columns}
    \begin{column}{0.6\textwidth}
        \begin{itemize}
            \item<1-> For a closed--open pipe, the impedance at the open end is zero when $\cot(kl) = 0 ~ \Leftrightarrow ~ kl = (2n - 1)\cdot \pi / 2, ~ \text{for } n = 1,2,\ldots$
            \begin{itemize}
                \item<2-> The case $n = 1$ corresponds to a frequency where the quarter wave length is the same as the pipe length
                \item<2-> Remember from the earlier lectures that these are the \textbf{resonance frequencies} of the open--closed pipe
            \end{itemize}
            \item<3-> On the other hand, the impedance $Z_o$ is highest (antiresonance) when $kl = n\cdot \pi/2,~ \text{for } n = 1,2,...$
        \end{itemize}
    \end{column}
    
    \begin{column}{0.35\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=1.2\textwidth]{pipe_resonances.png}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}

\subsection{Distributed impedance of the ear canal}
\begin{frame}{Ear canal}
    \begin{itemize}
        \item<1-> Like said earlier, the $1/4$-wave resonance of the open ear canal is at $f \approx 3,4 ~\text{kHz}$
        \item<2-> However, if a hearing aid is inserted in the ear canal, the impedance has to be evaluated at the point where the HA output is located
        \begin{itemize}
            \item If deeper in the ear canal $\rightarrow$ impedance minimum at a higher frequency
        \end{itemize}
    \end{itemize}
    \begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics<3->[width=\textwidth]{earcanal_impedance.png}
        \end{figure}
    \end{column}
    
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item<3-> In the figure impedance is evaluated \SI{14}{mm} from the TM
        \item<3-> Impedance minimum (D) at $f \approx 7~\text{kHz}$
        \item<3-> Antiresonance (E) at $f \approx 14 \text{ kHz}$
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\subsection{Hearing aid elements}
\begin{frame}{Hearing aid elements}
    \begin{columns}[T]
    \begin{column}{0.4\textwidth}
        \begin{itemize}
            \item Also hearing aids have many acoustic elements which behave like waveguides:
            \item Venting
            \item Sound bore
        \end{itemize}
    \end{column}
    
    \begin{column}{0.6\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=0.9\textwidth]{earmold.png}
        \end{figure}
    \end{column}
    \end{columns}
\end{frame}

\section{Stepped ducts}
\begin{frame}{Stepped ducts}
    \begin{itemize}
        \item A more general case of a boundary condition can be found by looking at a change in the pipe diameter
        \item Here, in an infinitely long pipe the area changes from $S_1$ to $S_2$ at $x = 0$
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{stepped_duct.png}
    \end{figure}
\end{frame}

\begin{frame}{Stepped ducts}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=1.2\textwidth]{stepped_duct.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item A sound coming from pipe segment 1 will have a pressure $p_i$ and volume velocity $U_i$
        \item At $x = 0$, part of the sound will be reflected, which results in a backward-travelling wave with pressure $p_r$ and volume velocity $U_r$
        \item Another part of the sound will be transmitted further, and will have pressure $p_t$ and volume velocity $U_t$
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\subsection{Transmission and reflection}
\begin{frame}{Stepped ducts}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=1.2\textwidth]{stepped_duct.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item At $x = 0$, pressure and vol velocity have to be continuous, and we have $p_t = p_i + p_r$ and $U_t = U_i + U_r$. From here we get:
        \begin{align*}
            U_t &= U_i + U_r \\
            \Leftrightarrow \frac{p_t}{Z_2} &= \frac{p_i}{Z_1} - \frac{p_r}{Z_1} \\
            \Leftrightarrow \frac{p_i + p_r}{Z_2} &= \frac{p_i - p_r}{Z_1} \\
            \Leftrightarrow Z_1 p_i + Z_1 p_r &= Z_2 p_i - Z_2 p_r \\
            \Leftrightarrow p_r(Z_2 + Z_1) &= p_i(Z_2 - Z_1) \\
            \Leftrightarrow \frac{p_r}{p_i} &= \frac{Z_2 - Z_1}{Z_2 + Z_1}
        \end{align*}
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Stepped ducts}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=1.2\textwidth]{stepped_duct.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item At $x = 0$, pressure and vol velocity have to be continuous, and we have $p_t = p_i + p_r$ and $U_t = U_i + U_r$. From here we get:
        \begin{align*}
            U_t &= U_i + U_r \\
            \Leftrightarrow \frac{p_t}{Z_2} &= \frac{p_i}{Z_1} - \frac{p_r}{Z_1} \\
            \Leftrightarrow \frac{p_t}{Z_2} &= \frac{p_i - (p_t - p_i)}{Z_1} \\
            \Leftrightarrow Z_1 p_t &= 2 Z_2 p_i - Z_2 p_t \\
            \Leftrightarrow (Z_2 + Z_1) p_t &= 2 Z_2 p_i \\
            \Leftrightarrow \frac{p_t}{p_i} &= \frac{2 Z_2}{Z_2 + Z_1}
        \end{align*}
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Stepped ducts}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=1.2\textwidth]{stepped_duct.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item The ratio $p_r / p_i$ between reflected and incident waves is the \textbf{reflection coefficient} $R$
        \item The ratio $p_t / p_i$ between transmitted and incident waves is the \textbf{transmission coefficient} $T$
        \item If we now substitute $Z_1 = \rho_0 c / S_1$ and $Z_2 = \rho_0 c / S_2$ to the expressions of $R$ and $T$, we get:
        \begin{align*}
            R &= \frac{Z_2 - Z_1}{Z_2 + Z_1} = \frac{S_1 - S_2}{S_1 + S_2} \\
            T &= \frac{2 Z_2}{Z_2 + Z_1} = \frac{2 S_1}{S_1 + S_2}
        \end{align*}
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\subsection{Impedance matching}
\begin{frame}{Impedance matching}
    \begin{itemize}
        \item<1-> From the reflection coefficient $R = \frac{Z_2 - Z_1}{Z_2 + Z_1}$ we see that if the impedances don't match, there will be reflection of sound
        \item<2-> In the obvious case where $Z_2 = Z_1$, there is no reflection, and all sound is transmitted forward
        \item<3-> In a hearing aid, this would mean that sound is transmitted to the ear less efficiently
        \item<4-> One way to decrease reflection is to implement a horn at the end of the sound bore
        \item<5-> Increasing the diameter stepwise or gradually results in a better impedance matching
    \end{itemize}
\end{frame}

\subsection{Acoustic filters}
\begin{frame}{Acoustic filters}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{acoustic_filter.png}
    \end{figure}
    \begin{itemize}
        \item<1-> $R$ and $T$ are independent of frequency
        \item<2-> But if we add another step to the pipe so that we have two steps at a distance $L$ from each other, we get an acoustic filter which is frequency-specific
    \end{itemize}
\end{frame}

\begin{frame}{Acoustic filters -- lowpass}
    \begin{itemize}
        \item If we choose $S_1  = S_3$, we get two special cases. Again, for $\lambda \ll L$ we can consider the component as a lumped element
    \end{itemize}
    
    \begin{columns}[T]
    \begin{column}{0.45\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{ac_lowpass_compliance.png}
    \end{figure}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,1) to[short, o-o] (3,1);
    \draw (1.5,1) to[C=\mbox{$C_a$}] (1.5,0);
    \draw (0,0) to[short, o-o] (3,0);
    \end{circuitikz}
    $$C_a = L\cdot\frac{S_1}{\rho_0 c^2}$$
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{ac_lowpass_inertance.png}
    \end{figure}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,1) to[short, o-] (1,1)
    to[cute inductor, l_=\mbox{$M_a = L\cdot\frac{\rho_0}{S_1}$}] (2,1) to[short, -o] (3,1);
    \draw (0,0) to[short, o-o] (3,0);
    \end{circuitikz}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Acoustic filters -- lowpass}
    \begin{itemize}
        \item Both the expanded and the constricted sections effectively give the same result: they let low frequencies pass and attenuate high frequencies
        \item The important parameters are the length $L$ of the section, which determines the shape of the frequency response, and the ratio $\frac{S_1}{S}$, which determines the maximum amount of attenuation
    \end{itemize}
\end{frame}

\section{Laboratory exercise}
\begin{frame}{Lab exercise}
    \begin{itemize}
        \item This Wednesday (\textbf{27/3/2019}) we will have the first lab exercise at 12--14, in the Audiology lab
        \begin{itemize}
            \item In the exercise we will investigate the effect of venting in an earmold
            \item Read pages 82--90 and 127--149 from Dillon's book
            \item Read the exercise guide (in BB)
        \end{itemize}
        \item Next Monday there will be \textbf{no lecture!}
        \begin{itemize}
            \item Next lecture will be \textbf{8/4/2019}
            \item Read chapter 9 (pp. 333--350)
        \end{itemize}
    \end{itemize}
\end{frame}

\end{document}
