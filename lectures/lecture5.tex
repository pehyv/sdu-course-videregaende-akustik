\documentclass[usenames,dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{icomma}
\usepackage{verbatim}
\usepackage{animate}
\usepackage{circuitikz}
\usetheme{AnnArbor}
\usecolortheme{seagull}

\graphicspath{ {lecture_5/} }

\ctikzset{bipoles/length=.8cm,bipoles/thickness=1}

\title{Lecture 5 --- Impedance}
\subtitle{Advanced Acoustics / Vidergående Akustik / F19}
\author{Petteri Hyvärinen}
\date{March 11th 2019}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}


\section{Impedance}
\subsection{Impedance}
\begin{frame}{Impedance}
\begin{itemize}
    \item<1-> \textbf{Impedance} is a measure that is used in many fields of physics to describe how a system (or a part of it) responds to excitation
    \begin{itemize}
        \item<2-> It can be roughly understood as ``how much a system \underline{opposes} an external action''
    \end{itemize}
    \item<3-> The reciprocal (inverse) of impedance is \textbf{admittance}
    \begin{itemize}
        \item<4-> $~$ ``How much a system \underline{gives in} to an external action''
    \end{itemize}
    \item<5-> Together, impedance and admittance are sometimes referred to as \textbf{immittance}
    \begin{itemize}
        \item<5-> \textit{e.g.} ``immittance testing'' in audiology
    \end{itemize}
    \item<6-> We are especially interested in \textit{electrical, mechanical,} and \textit{acoustic} impedance
    \begin{itemize}
        \item All of these are present in an electroacoustic device, such as a headphone or loudspeaker
    \end{itemize}
\end{itemize}
\end{frame}


\section{Electrical impedance}
\subsection{Electrical impedance}
\begin{frame}{Electrical impedance}
    \begin{block}{Ohm's law}
    In an electrical system, the relationship between the voltage $V$ measured across a component, and the current $I$ flowing through the component is governed by the component's impedance $Z$:
    $$V = Z \cdot I$$
    \end{block}
    \begin{itemize}
        \item<2-> The book uses the symbol $V$ for voltage, but usually $U$ is used instead to avoid confusion with the unit of voltage, which is \textit{volt [V], e.g.} 230 V or 15 kV
        \item<3-> The unit of current is \textit{ampere [A], e.g.} 10 A or 500 mA
        \item<4-> And the unit of impedance is \textit{ohm [}$\Omega$\textit{]}
    \end{itemize}
\end{frame}

\begin{frame}{Electrical impedance}
    \begin{block}{Resistance and reactance}
    Impedance $Z$ has two parts: \textbf{resistance} $R$ and \textbf{reactance} $X$.
    \end{block}
    \pause
    \begin{block}{Impedance as a complex number}
    When impedance is written as a complex number, resistance is the \textbf{real} part of impedance, and reactance is the \textbf{imaginary} part of impedance:
    $$Z = R + Xi$$
    \end{block}
\end{frame}

\subsection{Resistance}
\begin{frame}{Resistance}
    The simplest electrical component is \ldots
    \begin{block}{A \textbf{resistor}}
    \begin{itemize}
        \item Symbol: 
        \begin{circuitikz} \draw (0,0) to[ european resistor=$R_1$ ] (2,0); \end{circuitikz}
        or \begin{circuitikz} \draw (0,0) to[ american resistor=$R_2$ ] (2,0); \end{circuitikz}
        \item It restricts the current flowing through it
        \item This can be achieved \textit{e.g.} by constructing the resistor from a material that is not a perfect conductor
        \item The basic property of a resistor is resistance $R$, which has the unit \textit{ohm} [$\Omega$]
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Resistance}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,0) to[european controlled current source=$I$] (0,3)
    --
    (3,3) to[european resistor, l_=$R$, v^=$V$] (3,0)
    --
    (0,0);
    \end{circuitikz}
    \end{column}
    
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{dc_source_R.png}
    \end{figure}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Resistance -- explained}
    \begin{figure}
        \centering
        \includegraphics[height=0.8\textheight]{ohms_law.png}
    \end{figure}
\end{frame}


\subsection{Reactance}
\begin{frame}{Reactive electrical components}
    \begin{columns}[T]
    \begin{column}{0.45\textwidth}
    \begin{block}{A \textbf{condensator} (\textit{a.k.a.} capacitor)}
    \begin{itemize}
        \item Symbol: 
        \begin{circuitikz}[baseline=0] \draw (0,0) to[ C=$C_1$ ] (2,0); \end{circuitikz}
        \item Opposes \textbf{change in voltage} by storing energy in its electric field
        \item Basic property: \textbf{capacitance} $C$, which has the unit \textit{farad} [F]
        \item Capacitive reactance $$X_C = -\frac{1}{\omega C}$$
    \end{itemize}
    \end{block}
    \end{column}
    \pause
    \begin{column}{0.5\textwidth}
    \begin{block}{An \textbf{inductor}}
    \begin{itemize}
        \item Symbol:\newline
        \begin{circuitikz}[baseline=0] \draw (0,0) to[cute inductor=$L_1$ ] (2,0); \end{circuitikz}
        or \begin{circuitikz}[baseline=0] \draw (0,0) to[ american inductor=$L_2$ ] (2,0); \end{circuitikz}
        \item Opposes \textbf{change in current} by storing energy in its magnetic field
        \item Basic property: \textbf{inductance} $L$ which has the unit \textit{henry} [H]
        \item Inductive reactance $$X_L = \omega L$$
    \end{itemize}
    \end{block}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Reactance}
    \begin{itemize}
        \item<1-> Total reactance, combining capacitive and inductive reactances: $$X = X_L + X_C = \omega L - \frac{1}{\omega C}$$
        \item<2-> Reactive components can \textbf{store energy} and release it later
        \begin{itemize}
            \item They are said to have ``memory'', whereas a resistor does not have memory
        \end{itemize}
        \item<3-> Reactive components add sluggishness to the system
        \begin{itemize}
            \item Remember, they \textit{oppose the change} of something
            \item When dealing with reactance, we are interested in \emph{time-varying} signals, and usually analyse how the system responds to \textbf{harmonic} signals $$x(t) = A\cdot \cos(\omega t + \phi)$$
            \item Remember: $\omega = 2\pi\cdot f$
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Resistance, alternating current}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,0) to[european current source=$I_p \cos\omega t$] (0,3)
    --
    (3,3) to[european resistor, l_=$R$, v^=V] (3,0)
    --
    (0,0);
    \end{circuitikz}
    \begin{align*}
        I(t) &= I_p \cos\omega t \\
        V(t) = I(t)\cdot R &= I_p R \cos\omega t \\
        V_p &= I_p \cdot R
    \end{align*}
    \end{column}
    
    \begin{column}{0.5\textwidth}

    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{ac_source_R.png}
    \end{figure}
    \centering
    Voltage and current \textbf{in phase}
    \end{column}
    \end{columns}
\end{frame}


\begin{frame}{Reactance, alternating current}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,0) to[european current source=$I_p \cos\omega t$] (0,3)
    --
    (3,3) to[european resistor, l_=$Z$] (3,0)
    --
    (0,0);
    \draw node (A) at (4,3) {}
    node (B) at (4,0) {}
    (A) to[open, v^=$V$] (B);
    \draw (3,3) to[short, -*] (4,3);
    \draw (3,0) to[short, -*] (4,0);
    \end{circuitikz}
    \begin{align*}
        I(t) &= I_p \cos\omega t \\
        V(t) = I(t)\cdot Z &= I_p |Z| \cos(\omega t + \phi) \\
        V_p &= I_p\cdot |Z|
    \end{align*}
    \end{column}
    
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{ac_source_X.png}
    \end{figure}
    \centering
    Voltage and current \textbf{not in phase}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Reactance, alternating current}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{circuitikz}[line width=1pt]
    \draw (0,0) to[european current source=$I_p \sin\omega t$] (0,3)
    --
    (3,3) to[european resistor, l_=$R$] (3,2)
    to[C, l_=$C$] (3,1)
    to[cute inductor, l_=$L$] (3,0)
    --
    (0,0);
    \draw node (A) at (4,3) {}
    node (B) at (4,0) {}
    (A) to[open, v^=$V$] (B);
    \draw (3,3) to[short, -*] (4,3);
    \draw (3,0) to[short, -*] (4,0);
    \end{circuitikz}
    \begin{align*}
        I(t) &= I_p \cos\omega t \\
        V(t) = I(t)\cdot Z &= I_p |Z| \cos(\omega t + \phi) \\
        V_p &= I_p\cdot |Z|
    \end{align*}
    \end{column}
    
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{ac_source_X.png}
    \end{figure}
    \centering
    Voltage and current \textbf{not in phase}
    \end{column}
    \end{columns}
\end{frame}

\subsection{Impedance as a complex number}
\begin{frame}{Complex impedance}
    \begin{itemize}
        \item<1-> So, reactance can cause the voltage and current to go out of phase with each other. How do we explain that?
        \item<2-> Expressing impedance as a complex number, we get
        $$Z = R + iX = R + i(X_L + X_C) = R + iX_L + iX_C = R + i\omega L - i \frac{1}{\omega C}$$
        \item<3-> And in polar form, we have
        \begin{align*}
            r = |Z| &= \sqrt{R^2 + (X_C + X_L)^2} = \sqrt{R^2 + (\omega L - \frac{1}{\omega C})^2} \\
            \phi_Z &= \arctan\left(\frac{X}{R}\right) = \arctan\left(\frac{X_C + X_L}{R}\right)
        \end{align*}
        
        \item<4-> Let's draw the individual components on an Argand diagram to get an idea of what is happening
    \end{itemize}
\end{frame}

\begin{frame}{Complex impedance -- Argand diagram}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{complex_impedance.png}
    \end{figure}
\end{frame}

\subsection{Phasors}
\begin{frame}{Complex impedance and phasors}
    \begin{itemize}
        \item<1-> We know that multiplication with a complex number corresponds to rotation (and a scaling) in the complex plane
        \item<2-> Since we are using complex impedance, a very useful trick is to express the voltage and current with complex \textbf{phasors}
        \item<3-> A sinusoidal signal $A\cdot \cos(\omega t + \phi)$ can be interpreted as the real part of a complex phasor:
        $$A\cdot \cos(\omega t + \phi) = \operatorname{Re}\{I_p\cdot e^{i(\omega t + \phi)}\}$$
    \end{itemize}
\end{frame}


\begin{frame}{Phasor}
    \centering
    \animategraphics[loop,controls,height=0.7\textheight]{10}{phasor/frame-}{0}{47}
    \\{\tiny CC BY-SA 3.0, by Gonfer at English Wikipedia}
\end{frame}

\begin{frame}{Complex impedance and phasors, cont'd}
    \begin{itemize}
        \item<1-> Now, taking the formula $V(t) = Z\cdot I(t)$, and expressing $I(t)$ as a phasor, we get
        \begin{align*}
            V(t) &= (R + iX)\cdot I_p \cdot e^{i\omega t} = |Z|e^{i\phi_Z}\cdot I_p e^{i\omega t} \\
            &= |Z|\cdot I_p \cdot e^{i(\omega t + \phi_Z)}
        \end{align*}
        \item<2-> Here, we see that $V(t)$ is also (the real part of) a phasor
        \begin{itemize}
            \item Its magnitude is $|Z|\cdot I_p = V_p$
            \item and argument is $\omega t + \phi_Z$
        \end{itemize}
        \item<3-> \ldots meaning that there is now a phase difference of $\phi_Z$ between the voltage and current
        \begin{itemize}
            \item If $\phi_Z$ is negative voltage is said to \textit{lag}, and if $\phi_Z$ is positive voltage is said to \textit{lead}
        \end{itemize}
        \item<4-> The measured voltage signal is $$\operatorname{Re}\left\{ |Z|I_p e^{i(\omega t + \phi_Z)}\right\} = |Z|\cdot I_p \cdot \cos\left(\omega t + \phi_Z\right)$$
    \end{itemize}
\end{frame}

\begin{frame}{Two phasors, off phase}
    \begin{columns}
    \begin{column}{0.5\textwidth}
    \centering
    \animategraphics[loop,controls,height=0.8\textheight]{10}{phase_diff/phasor-}{0}{49}
    \end{column}
    
    \begin{column}{0.45\textwidth}
    \begin{block}{$V = Z\cdot I$}
    $$\color{blue}{V_p\cdot e^{i{\omega t + \phi}}} = \color{red}{|Z| \cdot e^{i\phi_Z}} \cdot \color{ForestGreen}{I_p\cdot e^{i{\omega t}}}$$
    \end{block}
    \end{column}
    \end{columns}
\end{frame}

\subsection{Power}
\begin{frame}{Power}
    \begin{itemize}
        \item<1-> The complex power dissipated by a system with reactive components is:
        \begin{align*}
        S &= V I^* = V_{RMS}\cdot e^{i(\omega t + \phi_Z)} \cdot I_{RMS} \cdot e^{-i\omega t} \\
        &= V_{RMS}\cdot I_{RMS} \cdot e^{i(\cancel{\omega t} + \phi_Z - \cancel{\omega t)}} \\
        &= V_{RMS}\cdot I_{RMS}\cdot e^{i\phi_Z}
        \end{align*}
        where $I^*$ denotes the complex conjugate of the current.
        \item<2->The power transferred by the source to the impedance is the real part of complex power (symbol $W$ used in the book, often $P$ in other texts) :
        $$W = \operatorname{Re}\{S\} = \operatorname{Re}\{V_{RMS}\cdot I_{RMS}\cdot e^{i\phi_Z}\} = V_{RMS}\cdot I_{RMS}\cdot \cos(\phi_Z)$$
    \end{itemize}
\end{frame}


\section{Electrical--mechanical equivalent circuits}
\begin{frame}{Equivalent circuits}
    \begin{itemize}
        \item<1-> We have now considered just the electrical impedance, but we would like to also work with mechanical and acoustic impedance
        \item<2-> Luckily, we can express mechanical systems as electrical \textbf{equivalent circuits}
        \item<3-> By ``equivalent'' it is meant that the two representations of the system are mathematically similar
        \item<4-> For example, if we treat mechanical \emph{force} as analogous to electrical \emph{voltage}, and mechanical \emph{velocity} as analogous to electrical \emph{current}, we get a so-called \textbf{impedance analogy}
    \end{itemize}
\end{frame}

\subsection{Mechanical component and their impedances}
\begin{frame}{Mechanical impedance}
    \begin{block}{Impedance analogy}
    $$Z_m = \frac{F}{u}$$. 
    \end{block}
    Compare this to electrical impedance: $$Z_e = \frac{U}{I}$$
\end{frame}

\begin{frame}{Mechanical components and their impedances}
    \begin{itemize}
        \item<1-> We will now go through the three basic mechanical components
        \item<2-> Again, we are considering the system's response to harmonic motion. This means that the \textbf{velocity} will be of the form: $$u(t) = u_p \cdot \cos(\omega t)$$
        \item<3-> From here, we can also derive \textbf{displacement}:
        $$\xi(t) = \int u(t) \,dt =  \frac{u_p}{\omega}\cdot sin(\omega t) = \frac{u_p}{\omega}\cos(\omega t - \pi/2)$$
        \item<4-> and \textbf{acceleration}:
        $$a(t) = \dot{u} = \frac{\partial u}{\partial t} = -\omega\cdot u_p \cdot \sin(\omega t) = \omega \cdot u_p \cdot \cos(\omega t + \pi/2)$$
    \end{itemize}
\end{frame}

\begin{frame}{Mechanical impedance -- dashpot}
        \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{damper.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.7\textwidth}
    \begin{itemize}
        \item<1-> For a dashpot, force and velocity are related like this: $$F = R\cdot u$$
        \item<2-> If we consider only harmonic motion, then by expressing velocity as a phasor $u = u_p e^{i\omega t}$ we get the mechanical impedance for a dashpot:
        \begin{align*}
            Z_m &= \frac{F}{u} = \frac{R\cdot \cancel{u_p\cdot e^{i\omega t}}}{\cancel{u_p\cdot e^{i\omega t}}} = R
        \end{align*}
        \item<3-> So the mechanical impedance of a dashpot is purely real
    \end{itemize}

    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Mechanical impedance -- dashpot}
        \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{damper.png}
    \end{figure}
    
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{damper_phase.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{block}{Mechanical impedance of a dashpot}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
        Z_m &= R
    \end{align*}
    \end{block}
    \begin{itemize}
        \item<2-> We see that a dashpot is similar to a resistor in the electrical domain (where $Z_e = R$, and $V = R\cdot I$)
        \item<3-> Because there is no reactance, the $F$ and $u$ are \textbf{in phase}
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}


\begin{frame}{Mechanical impedance -- mass}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{mass.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.7\textwidth}
    \begin{itemize}
        \item<1-> For a mass, force and velocity are related like this: $$F = m\cdot a = m\cdot \dot{u} = m\cdot \frac{\partial u}{\partial t}$$
        \item<2-> If we consider only harmonic motion, then by expressing velocity as a phasor $u = u_p e^{i\omega t}$ we get $\dot{u} = i\omega \cdot u_p\cdot  e^{i\omega t}$
        \item<3-> Now mechanical impedance for a mass can be written as:
        \begin{align*}
            Z_m &= \frac{F}{u} = \frac{m\cdot i\omega \cdot \cancel{u_p \cdot e^{i\omega t}}}{\cancel{u_p\cdot e^{i\omega t}}} = i\omega m
        \end{align*}
        \item<4-> This is purely \emph{reactive}, so we can write for a mass: $Z_m = i\cdot X_M$
    \end{itemize}

    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Mechanical impedance -- mass}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{mass.png}
    \end{figure}
    
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{mass_phase.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{block}{Mechanical impedance of a mass}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
        X_M &= \omega m \\
        \Leftrightarrow Z_M &= i\cdot X_M = i \omega m
    \end{align*}
    \textbf{Bemærk!} Here I use $Z_m$ for mechanical impedance in general, but $Z_M$ specifically for impedance of a mass.
    \end{block}
    \begin{itemize}
        \item<2-> We see that a mass is similar to an inductor in the electrical domain (where $Z_e = i\cdot X_L = i \omega L \quad \Rightarrow L \leftrightarrow m$)
        \item<3-> The reactance results in $F$ leading $u$ by a factor of $i$ ($\Rightarrow + \pi/2$)
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Mechanical impedance -- spring}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{spring.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.7\textwidth}
    \begin{itemize}
        \item<1-> For a spring, force and velocity are related like this: $$F = s\cdot \xi = s\cdot \int u \,dt$$
        \item<2-> If we consider only harmonic motion, then by expressing velocity as a phasor $u = u_p e^{i\omega t}$ we get $\int u \,dt = \frac{u_p}{i\omega} e^{i\omega t}$
        \item<3-> Mechanical impedance for a spring:
        \begin{align*}
            Z_m &= \frac{F}{u} = \frac{s\cdot\frac{\cancel{u_p}}{i\omega}\cancel{e^{i\omega t}}}{\cancel{u_p\cdot e^{i\omega t}}} = \frac{s}{i\omega} = -i\cdot\frac{s}{\omega}
        \end{align*}
        \item<4-> Again we can write for a spring: $Z_m = i\cdot X_S$
    \end{itemize}

    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Mechanical impedance -- spring}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{spring.png}
    \end{figure}
    
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{spring_phase.png}
    \end{figure}
    \end{column}
    
    \begin{column}{0.65\textwidth}
    \begin{block}{Mechanical impedance of a spring}
    \setlength\abovedisplayskip{0pt}
    \begin{align*}
        X_S &= -\frac{s}{\omega}\\
        \Leftrightarrow Z_S &= i\cdot X_S = -i \frac{s}{\omega}
    \end{align*}
    \end{block}
    \begin{itemize}
        \item<2-> We see that a spring is similar to a condensator in the electrical domain (where $Z_e = i\cdot X_C = -i \frac{1}{\omega C} \quad \Rightarrow C \leftrightarrow \frac{1}{s}$)
        \item<3-> The reactance results in $F$ lagging $u$ by a factor of $i$ ($\Rightarrow - \pi/2$)
    \end{itemize}
    \end{column}
    \end{columns}
\end{frame}


\subsection{Equivalent circuits of systems}
\ctikzset{bipoles/length=1cm,bipoles/thickness=1}
\begin{frame}{Electrical--mechanical analogies (impedance analogy)}
    \begin{center}
        \begin{tabular}{ c c c }
         \textbf{Mechanical} & & \textbf{Electrical} \\ 
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[mass=$m$ ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[cute inductor=$L$ ] (2,0); \end{circuitikz} \\
         Mass & & Inductance \\
         & &\\
         
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[spring=\mbox{$C_m=\frac{1}{s}$} ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[C=$C$ ] (2,0); \end{circuitikz} \\
         Compliance & & Capacitance \\
         & &\\
         
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[damper=$R_M$ ] (2,0); \end{circuitikz}
         &
         $\longleftrightarrow$
         &
         \begin{circuitikz}[baseline=0, line width=1pt] \draw (0,0) to[european resistor=$R$ ] (2,0); \end{circuitikz} \\
         Resistance & & Resistance
        \end{tabular}
    \end{center}
\end{frame}

\ctikzset{bipoles/length=.8cm,bipoles/thickness=1}
\begin{frame}{Impedance of a mechanical system -- series configuration}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{mechanical_series.png}
    \end{figure}
    \pause
    \end{column}
    
    \begin{column}{0.65\textwidth}
    For the equivalent circuit, those components that have the same velocity are connected in series. Here all parts have the same displacement and velocity, so the circuit according to an impedance analogy looks like this:\\
    \begin{center}
    \begin{circuitikz}[line width=1pt]
    \ctikzset{bipoles/length=.8cm,bipoles/thickness=1}
    \draw (0,0) to[european voltage source=$F$] (0,3)
    to[short, i^>=$u$]
    (3,3) to[european resistor, l=\mbox{$R = R_m$}] (3,2)
    to[C, l=\mbox{$C = 1/s$}] (3,1)
    to[cute inductor, l=\mbox{$L=m$}] (3,0)
    --
    (0,0);
    \end{circuitikz}
    \end{center}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Impedance of a mechanical system -- series configuration}
    \begin{itemize}
        \item<1-> For impedances connected in series (one after the other), the total impedance is their sum:
        $$Z = R + X_M + X_S = R - i\frac{s}{\omega} + i\omega m = R + i\left(\omega m  - \frac{s}{\omega}\right)$$
        \item<2-> The magnitude of the impedance is:
        $$|Z| = \sqrt{R^2 + \left(\omega m - \frac{s}{\omega}\right)^2}$$
        \item<3-> The smallest magnitude for the impedance occurs when the second term is zero, \emph{i.e.} at $$\omega m - s/\omega = 0 \quad \Leftrightarrow \quad \omega_0 = \sqrt{\frac{s}{m}}$$
    \end{itemize}
\end{frame}

\begin{frame}{Impedance of a mechanical system  -- series configuration}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\textwidth]{mechanical_series_resonance.png}
    \end{figure}
\end{frame}



\begin{frame}{Impedance of a mechanical system -- parallel configuration}
    \begin{columns}
    \begin{column}{0.3\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{mechanical_parallel.png}
    \end{figure}
    \pause
    \end{column}
    
    \begin{column}{0.65\textwidth}
    Now the spring and dashpot can have different displacements, but the same force is applied over them. The mass has the same velocity as the damper, so the equivalent circuit according to an impedance analogy is:\\
    \begin{center}
    \begin{circuitikz}[line width=1pt]
    \ctikzset{bipoles/length=.8cm,bipoles/thickness=1}
    \draw (0,0) to[european voltage source=$F$] (0,3)
    --
    (4,3) to[european resistor, l=\mbox{$R = R_m$}] (4,1.5)
    to[cute inductor, l=\mbox{$L = m$}] (4,0) -- (2,0);
    \draw (2,3) to[C, l=\mbox{$C=1/s$}] (2,0)
    --
    (0,0);
    \end{circuitikz}
    \end{center}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Impedance of a mechanical system -- parallel configuration}
    \begin{itemize}
        \item<1-> For impedances connected in parallel (side by side), the total impedance can be calculated from:
        $$\frac{1}{Z} = \frac{1}{X_M} + \frac{1}{R + X_S} = -\frac{\omega}{is} + \frac{1}{R + i\omega m} = Y$$
        \item<2-> Here $Y$ is the \textbf{admittance}, the reciprocal of impedance, which is used here to make the calculations easier
        \item<3-> With a bit of algebra, we get
        $$Y = \frac{R}{R^2 + \omega^2 m^2} + i\cdot \frac{\omega^3 m^2 / s + \omega R^2 /s - \omega m}{R^2 + \omega^2 m^2}$$
        \item<4-> The minimum of $|Y|$ (\emph{i.e.} maximum of $|Z| \rightarrow$ \emph{antiresonance}) occurs at $\operatorname{Im}\{Y\} = 0$ $$\Rightarrow \quad \omega^3 m^2 / s + \omega R^2/s - \omega m = 0 \quad \Leftrightarrow \quad \omega_0 = \sqrt{\frac{s}{m} - \frac{R^2}{m^2}}$$
    \end{itemize}
\end{frame}

\begin{frame}{Impedance of a mechanical system  -- parallel configuration}
    \begin{figure}
        \centering
        \includegraphics[width=0.45\textwidth]{mechanical_parallel_antiresonance.png}
    \end{figure}
\end{frame}

\section{Thank you}
\begin{frame}{Next week}
    \begin{itemize}
        \item Next week the topics will be \textbf{specific acoustic impedance} (pp. 241--246) and \textbf{acoustic impedance and admittance} (pp. 246--273)
    \end{itemize}
\end{frame}

\end{document}
